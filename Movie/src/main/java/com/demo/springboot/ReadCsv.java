package com.demo.springboot;

import com.demo.springboot.domain.dto.MovieDto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ReadCsv {

    private static final String SEMICOLON_DELIMITER = ";";

    List<MovieDto> movies = new ArrayList<>();

    public List<MovieDto> readFromCsv() {
        try (Scanner scanner = new Scanner(new File("./src/main/resources/movies.csv"))) {
            scanner.useDelimiter(SEMICOLON_DELIMITER);
            int index = 0;
            while (scanner.hasNextLine()) {
                MovieDto movieDto = new MovieDto(scanner.next(), scanner.next(), scanner.next(), scanner.next().trim());
                movies.add(movieDto);
                if (index < 1) {
                    movies.remove(0);
                }
                index++;
            }

        } catch (Exception e) {
        }
        return movies;
    }
}
